package pro.siberian.qa.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pro.siberian.qa.helper.WebHelper;
import pro.siberian.qa.test.web.WebTest;

/**
 * Created by user on 13.06.2018.
 */
public class DashboardPage {
/*
    public DashboardPage(){
        PageFactory.initElements(driver, this);
    }
*/
    @FindBy(css = ".main-title.dashboard__title")
    private WebElement dashboardMainTitle;

    public String GetMainTitleText() {
        return  dashboardMainTitle.getText();
    }
/*
    public boolean isDashboardExist(){
        return WebHelper.isElementPresent(driver,  By.cssSelector(".main-title.dashboard__title"));
    }
*/
}
