package pro.siberian.qa.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pro.siberian.qa.helper.WebHelper;
import pro.siberian.qa.test.web.WebTest;

public class LoginPage {
/*
    public LoginPage(){
        PageFactory.initElements(driver, this);
    }
*/
    public String LoginPageURL  = "http://residenz-stage.siberian.pro/login";
    public String LogoutPageURL = "http://residenz-stage.siberian.pro/logout";

    @FindBy(id = "username")
    private WebElement loginField;

    @FindBy(id = "password")
    private WebElement passwordField;

    @FindBy(xpath = "//input[@value='Log in']")
    private WebElement loginButton;


    public String getLoginPlaceholder(){
        String  placeholder = loginField.getAttribute("placeholder");
        return placeholder;
    }

    public String getPasswordPlaceholder(){
        String  placeholder = passwordField.getAttribute("placeholder");
        return placeholder;
    }

    public void setLogin(String login) {
        loginField.sendKeys(login);
    }

    public void setPassword(String password) {
        passwordField.sendKeys(password);
    }

    public String getLogin() {
        return loginField.getAttribute("value");
    }

    public String getPassword() {
        return passwordField.getAttribute("value");
    }

    public String getLoginBorderColor(){
        return loginField.getCssValue("background-color");
    }
    public String getPasswordBorderColor(){
        return passwordField.getCssValue("background-color");
    }


    public void pressButtonLogin() {
        loginButton.click();
    }

}