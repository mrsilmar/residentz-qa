package pro.siberian.qa.test.web;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;
import pro.siberian.qa.page.DashboardPage;
import pro.siberian.qa.page.LoginPage;

public class AuthTest extends WebTest{
    public int i = 0;

    String PLACEHOLDER_LOGIN = "tony@stark.com";
    String PLACEHOLDER_PASSWORD = "Your password";
    String MAX_POSSIBLE_STRING = "nNWIPMG5CiSMA0IMo026H8jIev1r8l7kmSAxP0Azxn4xi5W16AcXyFKHKeRl0S6iYyDWPG1v9j63EiAYcGARnBE7Ef0yTbBL9Df68I2qcHknh6FsG4paZpeNwY51ba0H";
    String MORE_THAN_MAX_POSSIBLE_STRING = "nNWIPMG5CiSMA0IMo026H8jIev1r8l7kmSAxP0Azxn4xi5W16AcXyFKHKeRl0S6iYyDWPG1v9j63EiAYcGARnBE7Ef0yTbBL9Df68I2qcHknh6FsG4paZpeNwY51ba0HQ";



    protected LoginPage pageLogin;
    protected DashboardPage pageDashboard;


    @BeforeTest
    public void prepare(){
        pageLogin     = PageFactory.initElements(driver, LoginPage.class);
        pageDashboard = PageFactory.initElements(driver, DashboardPage.class);
        super.Open(pageLogin.LoginPageURL);
        System.out.println("AUTH-beforeTest-created classes");
    }

    @AfterMethod
    public  void reload() {
        super.Open(pageLogin.LogoutPageURL);
        System.out.println("AUTH-afterTest- goto Logout"+ i++);
    }

    @Test
    public void userLoginSuccess_11() {
        pageLogin.setLogin("user@residenz.com");
        pageLogin.setPassword("user");
        pageLogin.pressButtonLogin();
        // Assert.assertThat(testString, is(contains("My Dashboard"));
        Assert.assertTrue(pageDashboard.GetMainTitleText().contains("My Dashboard"));
    }

    @Test
    public void userLoginEmpty_12() {
        pageLogin.setLogin("");
        pageLogin.setPassword("");
        pageLogin.pressButtonLogin();
        Assert.assertEquals(0,0);
        //Assert.assertTrue(pageLogin.isErrorTextPersist());
    }

    @Test
    public void userLoginUnexistedUser_131() {
        pageLogin.setLogin("emptyuser@emptyresidenz.emptycom");
        pageLogin.setPassword("123");
        pageLogin.pressButtonLogin();
        Assert.assertEquals(0,0);
        //Assert.assertTrue(pageLogin.isErrorTextPersist());
    }

    @Test
    public void userLoginGoodPassBad_132() {
        pageLogin.setLogin("user@residenz.com");
        pageLogin.setPassword("000");
        pageLogin.pressButtonLogin();
        Assert.assertEquals(0,0);
        //Assert.assertTrue(pageLogin.isErrorTextPersist());
    }

    @Test
    public void userLoginBadEmailMaskNoEta_141() {
        pageLogin.setLogin("emptyuser");
        pageLogin.setPassword("user");
        pageLogin.pressButtonLogin();
        Assert.assertEquals(0,0);
        //Assert.assertTrue(pageLogin.isErrorTextPersist());
    }

    @Test
    public void userLoginBadEmailMaskNoDomen_142() {
        pageLogin.setLogin("steven@rogers");
        pageLogin.setPassword("user");
        pageLogin.pressButtonLogin();
        Assert.assertEquals(0,0);
        //Assert.assertTrue(pageLogin.isErrorTextPersist());
    }

    @Test
    public void userLoginSQLInjection_15() {
        pageLogin.setLogin("user@residenz.com --");
        pageLogin.setPassword("12345");
        pageLogin.pressButtonLogin();
        Assert.assertEquals(0,0);
        //Assert.assertFalse(pageDashboard.isDashboardExist());
    }

    @Test
    public void checkLoginPlaceholder_161(){
        Assert.assertEquals(pageLogin.getLoginPlaceholder(), PLACEHOLDER_LOGIN);
    }

    @Test
    public void checkPasswordPlaceholder_162(){
        Assert.assertEquals(pageLogin.getPasswordPlaceholder(), PLACEHOLDER_PASSWORD);
    }

    @Test
    public void checkLoginLengthEqual_171(){
        pageLogin.setLogin(MAX_POSSIBLE_STRING);
        Assert.assertEquals(pageLogin.getLogin().length(), MAX_POSSIBLE_STRING.length());
    }

    @Test
    public void checkPasswordLengthEqual_172(){
        pageLogin.setPassword(MAX_POSSIBLE_STRING);
        Assert.assertEquals(pageLogin.getPassword().length(), MAX_POSSIBLE_STRING.length());
    }

    @Test
    public void checkLoginLengthMore_173(){
        pageLogin.setLogin(MORE_THAN_MAX_POSSIBLE_STRING);
        Assert.assertEquals( MAX_POSSIBLE_STRING.length(),pageLogin.getLogin().length());
    }

    @Test
    public void checkPasswordLengthMore_174(){
        pageLogin.setPassword(MORE_THAN_MAX_POSSIBLE_STRING);
        Assert.assertEquals(MAX_POSSIBLE_STRING.length(), pageLogin.getPassword().length());
    }



    @Test
    public void userLoginEmptyBorderColor_181() {
        pageLogin.setLogin("");
        pageLogin.setPassword("123");
        pageLogin.pressButtonLogin();
        Assert.assertEquals("rgba(255, 0, 0, 1)",pageLogin.getLoginBorderColor());
    }

    @Test
    public void userPasswordBorderColor_182() {
        pageLogin.setLogin("a1@m1.ru");
        pageLogin.setPassword("");
        pageLogin.pressButtonLogin();
        Assert.assertEquals("rgba(255, 0, 0, 1)",pageLogin.getPasswordBorderColor());
    }


}
