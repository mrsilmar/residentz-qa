package pro.siberian.qa.test.web;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.sql.Driver;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.*;

/**
 * Created by user on 13.06.2018.
 */
public class WebTest {
    private WebDriver driverChrome;
    private  WebDriver driverFireFox;
    private  WebDriver driverEdge;
    protected  WebDriver driver;




    @Parameters("browser")
    @BeforeTest
    protected  WebDriver getWebDriver(String browser) {
        if(browser.equals("chrome")) {
            if (driverChrome == null) {
                System.setProperty("webdriver.chrome.driver", "D:\\Home\\GitRepo\\qa\\chromedriver.exe");
                ChromeOptions option = new ChromeOptions();
                option.addArguments("start-maximized");
                driverChrome = new ChromeDriver(option);
            }
            driver = driverChrome;
        } else if(browser.equals("firefox")) {
            if (driverFireFox == null) {
                System.setProperty("webdriver.gecko.driver", "D:\\Home\\GitRepo\\qa\\geckodriver.exe");
                driverFireFox = new FirefoxDriver();
            }
            driver = driverFireFox;
        }    else if(browser.equals("edge")) {
        if (driverEdge== null) {
            System.setProperty("webdriver.edge.driver", "D:\\Home\\GitRepo\\qa\\MicrosoftWebDriver.exe");
            driverEdge = new EdgeDriver();
        }
        driver = driverEdge;
    }

        driver.manage().window().maximize();
        //driver.manage().timeouts().implicitlyWait(timeoutTime, TimeUnit.SECONDS);
        System.out.println("WebTest-beforeTest- choose driver");
        return driver;

    }


    protected void Open(String adressToOpen) {

            driver.get(adressToOpen);
            //driver.manage().timeouts().implicitlyWait(timeoutTime, TimeUnit.SECONDS);

    }

    @AfterTest
    protected void Quit() {
        System.out.println("WebTest-aftetTest- quit driver");

        if(driverFireFox != null) {
            System.out.println("driverFireFox quited");
            driverFireFox.quit();
        } else{
            System.out.println("driverFireFox is null");
        }

        if(driverChrome != null) {
            System.out.println("driverChrome quited");
            driverChrome.quit();
        } else{
            System.out.println("driverChrome is null");
        }

        if(driver != null) {
            System.out.println("drive quited");
            driver.quit();
        } else{
            System.out.println("driver is null");
        }

        if(driverEdge != null) {
            System.out.println("driveEdge quited");
            driverEdge.quit();
        } else{
            System.out.println("driverEdge is null");
        }



    }


}
